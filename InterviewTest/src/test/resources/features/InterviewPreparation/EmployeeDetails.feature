@EmployeeDetails
Feature: Interview

  Scenario: verify all the data related to employees is present
    Given I execute get APi call for the employees
    Then I verify api call is success

  Scenario: verify by reading the value from excel
    Given Fetch a single employee record using excel or CSV as external file
    Then I verify api call for individual employees