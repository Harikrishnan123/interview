
package runner;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        features = "classpath:features",
        glue = {"com/api/stepdef"},
        tags = {"@EmployeeDetails"},
        plugin = {"html:target/cucumber-html-report","pretty",
                "com.cucumber.listener.ExtentCucumberFormatter:output/report.html","json:target/cucumber/cucumber.json"})

public class CucumberRunner extends AbstractTestNGCucumberTests {
        @AfterClass
        public static void writeExtentReport() {
                Reporter.loadXMLConfig(new File("C:\\UST-GLOBAL\\GIT-PROJECT\\InterviewTest\\extent-config.xml"));
                Reporter.setSystemInfo("User Name", "Hari");
                Reporter.setSystemInfo("Application Name", "Test App");
                Reporter.setTestRunnerOutput("Test Execution Cucumber Report");   }

}