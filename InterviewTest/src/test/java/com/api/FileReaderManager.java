package com.api;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FileReaderManager {

public static String ReadProperty(String Key) {
    try {
        InputStream input = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\resources\\Configuration.properties");
        Properties prop = new Properties();
        prop.load(input);
        Key = prop.getProperty(Key);
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return Key;
}
}