package com.api;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class Services {
    private static final OkHttpClient client = new OkHttpClient();
    public static Response sendPostRequest(String Payload, String uri, Map<String, String> sendheaders) {
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), Payload);
        Request request;
        if (sendheaders == null) {
            request = (new Request.Builder()).url(uri).post(body).build();
        } else {

            Headers headerBuild = Headers.of(sendheaders);
            request = (new Request.Builder()).url(uri).headers(headerBuild).post(body).build();
        }

        Response res = null;

        try {
            res = client.newCall(request).execute();
        } catch (IOException var7) {
            var7.printStackTrace();
        }

        return res;
    }

    public static Response getRequest( String uri) {
        Request  request = (new Request.Builder()).url(uri).build();
        Response res = null;
        try {
            res = client.newCall(request).execute();

        } catch (IOException var7) {
            var7.printStackTrace();
        }

        return res;
    }

    public static JsonNode getJsonNodeFromResponse(Response response) {
        JsonNode jsonNode = null;
        String jsonData = "";

        try {
            jsonData = response.body().string();
            jsonNode = (new ObjectMapper()).readTree(jsonData);
            return jsonNode;
        } catch (Exception var4) {

        }
        return jsonNode;
    }
}
