package com.api.stepdef;

import com.api.FileReaderManager;
import com.api.Readexcel;
import com.api.Services;
import com.aventstack.extentreports.Status;
import com.cucumber.listener.Reporter;
import com.fasterxml.jackson.databind.JsonNode;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import okhttp3.Response;
import org.testng.Assert;

public class Testcases {
    Response response = null;
    public static JsonNode jsonObject;

    @Given("^I execute get APi call for the employees$")
    public void getApiCallEmployee() {
        response=  Services.getRequest(FileReaderManager.ReadProperty("Api_URL_Multiple"));
        Reporter.addStepLog(Status.PASS+"getting the response  :"+response);
    }

    @Given("^Fetch a single employee record using excel or CSV as external file$")
    public void getValueExcel() {
        Readexcel.readData();
        FileReaderManager s = new FileReaderManager();
        String Url = FileReaderManager.ReadProperty("Api_URL_Single")+"/"+Readexcel.ReadData;
        response=  Services.getRequest(Url);
    }

    @Then("^I verify api call is success$")
    public void verifyTheStatusCall() {
        if(response.code()==200) {
            Assert.assertEquals(response.code(), 200, "Employee api execute successfully");
            Reporter.addStepLog(Status.PASS+" Response code  :"+response.code());
            jsonObject = Services.getJsonNodeFromResponse(response);
            Reporter.addStepLog(Status.PASS+" Response   :"+jsonObject);
            int Size = jsonObject.get("data").size();
            for(int i=1;i<=Size-1;i++)
            {
                JsonNode Image = jsonObject.get("data").get(i).get("profile_image");
                if(Image.equals("\\\"\\\""))
                    Assert.fail();
                else
                    Assert.assertEquals("", "", "Profile Image is empty");
                Reporter.addStepLog(Status.PASS+" Image   :"+Image);
            }
        }
        else
        {
            Assert.assertEquals(response.code(), 200, "Employee api failed");
            Reporter.addStepLog(Status.FAIL+" Response code  :"+response.code());
        }



    }

    @Then("^I verify api call for individual employees$")
    public void verifyIndividualStatusCall()
    {
        if(response.code()==200) {
            Assert.assertEquals(response.code(), 200, "Employee api execute successfully");
            Reporter.addStepLog(Status.PASS + " Response code  :" + response.code());

            jsonObject = Services.getJsonNodeFromResponse(response);
            Reporter.addStepLog(Status.PASS + " Response   :" + jsonObject);
            String ID, employee_name, employee_salary, employee_age, message, status, profile_image;
            ID = jsonObject.get("data").get("id").toString();
            employee_name = jsonObject.get("data").get("employee_name").toString().replace("\"", "");
            employee_salary = jsonObject.get("data").get("employee_salary").toString();
            employee_age = jsonObject.get("data").get("employee_age").toString();
            message = jsonObject.get("message").toString().replace("\"", "");
            status = jsonObject.get("status").toString().replace("\"", "");
            profile_image = jsonObject.get("status").toString().replace("\"\"", "");
            Assert.assertEquals(status, "success", "success status received");
            Assert.assertNotNull(ID);
            Assert.assertNotNull(employee_name);
            Assert.assertNotNull(employee_age);
            Assert.assertNotNull(employee_salary);
            Assert.assertNotNull(profile_image);
            Assert.assertEquals(message, "Successfully! Record has been fetched.", "Successfully got the sucess message ");
            Reporter.addStepLog(Status.PASS + " ID   :" + ID + "-employee_name: " + employee_name + "employee_salary: " + employee_salary + "employee_age: " + employee_age + "message: " + message + "status: " + status);
        }
        else
        {
            Assert.assertEquals(response.code(), 200, "Employee api Failed");
            Reporter.addStepLog(Status.FAIL+" Response code  :"+response.code());
        }
    }
}


